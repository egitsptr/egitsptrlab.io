![Preview](/capture.png)

Clone for locally

0. Prepare with [Ruby](https://www.ruby-lang.org/)
1. Start by [installing Bundler](http://bundler.io) `gem install bundler`
2. Then run `bundle install` to get Jekyll and all the dependencies.
3. Edit _config.yml as needed.
4. Run the Jekyll server with `bundle exec jekyll serve`
5. Go to http://localhost:4000